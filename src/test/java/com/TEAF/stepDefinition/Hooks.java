package com.TEAF.stepDefinition;

import cucumber.api.Scenario;
import cucumber.api.java.Before;

public class Hooks {

	public static Scenario scenario;

	@Before
	public void beforeHook(Scenario scenario) {
		this.scenario = scenario;
	}

}
