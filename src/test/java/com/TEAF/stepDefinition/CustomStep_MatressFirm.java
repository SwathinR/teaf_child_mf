package com.TEAF.stepDefinition;

import static org.junit.Assert.assertFalse;
import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.ComparisonFailure;

import com.TEAF.Hooks.Hooks;
import com.TEAF.framework.API_UD;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.RestAssuredUtility;
import com.jayway.jsonpath.Configuration;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import net.bytebuddy.utility.RandomString;

public class CustomStep_MatressFirm {

	static Logger log = Logger.getLogger(CustomStep_MatressFirm.class.getName());

	
	
	RestAssuredUtility Rest = new RestAssuredUtility();
	API_UD ap = new API_UD();

	public static List<String> resultList = new ArrayList<String>();
	
	@Given("^URL '(.*)'$")
	public void URL(String url) {
		HashMapContainer.add("$$URL", url);
	}

	@Given("EndPoint {string}")
	public void endPoint(String point) {
		String URL = HashMapContainer.get("$$URL");

		ap.setPath(URL + point);
	}
	@Then("^Statuscode '(.*)'$")
	public void statuscode(int Expected) {

		int Actual = ap.getRes().andReturn().statusCode();
		try {
			org.junit.Assert.assertEquals("Status code Validation", Expected, Actual);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
			com.TEAF.stepDefinition.Hooks.scenario.write("\t---------------------ResponseData--------------------------------");
			com.TEAF.stepDefinition.Hooks.scenario.write(String.valueOf("\tHTTP Response Status Code: "+ap.getRes().getStatusCode()));
			com.TEAF.stepDefinition.Hooks.scenario.write("\tResponse JSON Body: \n"+ap.getRes().getBody().asString());
			com.TEAF.stepDefinition.Hooks.scenario.write("\t-----------------------------------------------------------------");
			com.TEAF.stepDefinition.Hooks.scenario.write("\n\n\n");
			org.junit.Assert.fail(e.getMessage());

		}
	}

	@Then("Match JSONPath {string} contains {string}")
	public void match_JSONPath_contains(String path, String Expected) {
		if (Expected.startsWith("$$")) {
			Expected = HashMapContainer.get(Expected);
		}

		String Actual = ap.getRes().getBody().asString();
		Object document = Configuration.defaultConfiguration().jsonProvider().parse(Actual);

		try {
			List<Object> actual;

			actual = com.jayway.jsonpath.JsonPath.read(document, path);
			if (actual.size()==1) {
				String singledata = String.valueOf(actual.get(0));
				org.junit.Assert.assertTrue(actual.toString().contains(singledata));

			}
			org.junit.Assert.assertTrue(actual.toString().contains(Expected));


		} catch (Throwable e) {
			String actual;

			actual = String.valueOf(com.jayway.jsonpath.JsonPath.read(document, path));
			try {
				org.junit.Assert.assertTrue(actual.toString().contains(Expected));
			} catch (Throwable e1) {
				// TODO Auto-generated catch block
				log.error(e1.getMessage());
				com.TEAF.stepDefinition.Hooks.scenario.write("\t---------------------ResponseData--------------------------------");
				com.TEAF.stepDefinition.Hooks.scenario.write(String.valueOf("\tHTTP Response Status Code: "+ap.getRes().getStatusCode()));
				com.TEAF.stepDefinition.Hooks.scenario.write("\tResponse JSON Body: \n"+ap.getRes().getBody().asString());
				com.TEAF.stepDefinition.Hooks.scenario.write("\t-----------------------------------------------------------------");
				com.TEAF.stepDefinition.Hooks.scenario.write("\n\n\n");
				org.junit.Assert.fail(" Comparison Failure : Expected "+Expected +" with Actual"+actual.toString() );
			}

		}

	}

	@When("^Print response in report$")
	public void print_response_in_report() {
		com.TEAF.stepDefinition.Hooks.scenario.write("\t---------------------ResponseData--------------------------------");
		com.TEAF.stepDefinition.Hooks.scenario.write(String.valueOf("\tHTTP Response Status Code: "+ap.getRes().getStatusCode()));
		com.TEAF.stepDefinition.Hooks.scenario.write("\tResponse JSON Body: \n"+ap.getRes().getBody().asString());
		com.TEAF.stepDefinition.Hooks.scenario.write("\t-----------------------------------------------------------------");
		com.TEAF.stepDefinition.Hooks.scenario.write("\n\n\n");
		
	}
	
	@Then("^Path '(.*)' is present in Response$")
	public void path_present_in_response(String path) {
		ResponseBody body = ap.getRes().getBody();
		JsonPath jsonPath = body.jsonPath();
		String obj;
		try {
			obj = jsonPath.get(path);
		} catch (Exception e) {
			obj = null;
		}
		try {
			org.junit.Assert.assertNotNull(obj);
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			log.error(e.getMessage());
			com.TEAF.stepDefinition.Hooks.scenario.write("\t---------------------ResponseData--------------------------------");
			com.TEAF.stepDefinition.Hooks.scenario.write(String.valueOf("\tHTTP Response Status Code: "+ap.getRes().getStatusCode()));
			com.TEAF.stepDefinition.Hooks.scenario.write("\tResponse JSON Body: \n"+ap.getRes().getBody().asString());
			com.TEAF.stepDefinition.Hooks.scenario.write("\t-----------------------------------------------------------------");
			com.TEAF.stepDefinition.Hooks.scenario.write("\n\n\n");
			org.junit.Assert.fail(e.getMessage());

		}

	}

	@When("Method {string}")
	public void Method(String Method_Name) {
		String Path = ap.getPath();
		RequestSpecification spec = ap.getReq();
		Response GetResponse = null;
		if(Method_Name.equals("Get")){
			GetResponse = Rest.Get(Path, spec);
		}else if(Method_Name.equals("Delete")){
			GetResponse = Rest.Delete(Path, spec);
		}else if(Method_Name.equals("Post")){
			GetResponse = Rest.Post(Path, spec);
		}else if(Method_Name.equals("Put")){
			GetResponse = Rest.Post(Path, spec);
		}
		ap.setRes(GetResponse);
	}

	@Then("Print response")
	public void print_response() {
		log.info("Response:" + ap.getRes().getBody().prettyPrint());
	}

	@Given("Set Header key {string} & {string} values")
	public void header_key_value(String string, String string2) throws Exception {
		if (string2.startsWith("@")) {
			String f = System.getProperty("user.dir") + "\\src\\test\\java\\com\\TEAF\\json\\" + string2.substring(1);
			string2 = FileUtils.readFileToString(new File(f), StandardCharsets.UTF_8);

		}
//		RequestSpecification req = ap.getReq();
		
	RequestSpecification spec = ap.getReq().header("Content-Type", ContentType.JSON, "Accept", ContentType.JSON)
				.header("client_id", "5e6f6c4de65f413eb056339c0f432b8f")
				.header("client_secret", "F35B75632964458d9525607eFCA0A1cb");

		ap.setReq(spec);
	}

	@Given("Content type {string}")
	public void content_type(String string) {
		RequestSpecification spec = ap.getReq().contentType(string);
		ap.setReq(spec);
	}
	
	Hooks hk = new Hooks();
	


	@And("^Query params with Key '(.*)' and value '(.*)'$")
	public void updatequeryParams(String key, String value) {
		if (!value.equals("Not Specified")) {
			RequestSpecification res = ap.getReq().queryParam(key, value);
			ap.setReq(res);
		}
		
	}
	
	@And("^Query params for random tid$")
	public void updatequeryParamsforTid() {
	
	String random = RandomString.make(6);
	String string = LocalTime.now().toString().replace(":", "").replace(".", "");
	String randomString = random+ string;
	com.TEAF.stepDefinition.Hooks.scenario.write("\t Generated Tid:" +randomString);
	RequestSpecification param = ap.getReq().param("tid", randomString);
	ap.setReq(param);

	}
	
	@When("^I read data from file '(.*)' & send request$")
	public void i_read_data_from_file(String fileName) throws Throwable {
		
		List<String> resultlist = new ArrayList<String>();
		String isfailure="false";
		
		File of = new File(System.getProperty("user.dir")+"/src/test/java/com/TEAF/json/"+fileName+".xlsx");
		FileInputStream ofin = new FileInputStream(of);
		Workbook owb = new XSSFWorkbook(ofin);
		Sheet sheet1 = owb.getSheetAt(0);

		// RestAssuredSteps rs = new RestAssuredSteps();

		Row headerRow = sheet1.getRow(0);
		List<Integer> paramCount = new ArrayList<Integer>();
		for (int j = 0; j < headerRow.getPhysicalNumberOfCells(); j++) {
			String headerCell = headerRow.getCell(j).getStringCellValue();
			if (headerCell.startsWith("Param_")) {
				paramCount.add(j);
			}
		}
		
		

		for (int i = 1; i < sheet1.getPhysicalNumberOfRows(); i++) {
			com.TEAF.stepDefinition.Hooks.scenario.write("\t|"+i+"| -----------------Param Key & Values------------------");
			RequestSpecification requestSpec = ap.getReq();
			if (requestSpec!=null) {
				log.debug("Object cleaned");
				requestSpec=null;
				requestSpec = RestAssured.given();
			}

			RequestSpecification specheader = requestSpec.header("Content-Type", ContentType.JSON, "Accept", ContentType.JSON)
					.header("client_id", "5e6f6c4de65f413eb056339c0f432b8f")
					.header("client_secret", "F35B75632964458d9525607eFCA0A1cb");

			ap.setReq(specheader);
			for (int j = 0; j < paramCount.size(); j++) {

				String enablevalue;
				try {
					enablevalue = sheet1.getRow(i).getCell(paramCount.get(j)).getStringCellValue();
				} catch (NullPointerException e) {
					enablevalue="";
				}
				
				
				//if (enablevalue.contains("Yes") || enablevalue.equalsIgnoreCase("yes")) {
				
				if (enablevalue.length()>1) {
					RequestSpecification req = ap.getReq();
					Req_UD rq= new Req_UD();
					
					String headervalue = sheet1.getRow(0).getCell(paramCount.get(j)).getStringCellValue();
					String[] split = headervalue.split("_");
					String paramvalue = sheet1.getRow(i).getCell(paramCount.get(j)).getStringCellValue();
					
					RequestSpecification param = req.param(split[1], paramvalue);
					
					resultList.add(headervalue+paramvalue);
					
					com.TEAF.stepDefinition.Hooks.scenario.write("\t"+headervalue+": "+paramvalue);
					rq.setReq(param);
					ap.setReq(rq.getReq());
					
					
				}

				
			}
			
		
			com.TEAF.stepDefinition.Hooks.scenario.write("\t"+"-----------------------------------------------------");	
			
			String Path = ap.getPath();
			RequestSpecification spec = ap.getReq();
			Response get = Rest.Get(Path, spec);
			ap.setRes(get);
			//log.debug(get.getStatusCode());
			//log.debug(get.getBody().asString());
			com.TEAF.stepDefinition.Hooks.scenario.write("\t---------------------ResponseData--------------------------------");
			com.TEAF.stepDefinition.Hooks.scenario.write(String.valueOf("\tHTTP Response Status Code: "+get.getStatusCode()));
			com.TEAF.stepDefinition.Hooks.scenario.write("\tResponse JSON Body: \n"+get.getBody().asString());
			com.TEAF.stepDefinition.Hooks.scenario.write("\t-----------------------------------------------------------------");
			com.TEAF.stepDefinition.Hooks.scenario.write("\n\n\n");
			resultList.add(String.valueOf(get.getStatusCode()));
			resultList.add(get.getBody().asString());

			String actualHtmlcode = String.valueOf(get.getStatusCode());
			long lcode = (long)sheet1.getRow(i).getCell(5).getNumericCellValue();
			String expectedstatusCode = String.valueOf(lcode);
			log.debug("Expected Html code"+ expectedstatusCode);
			log.debug("Actual Html code"+ actualHtmlcode);

			try {
				org.junit.Assert.assertEquals(expectedstatusCode, actualHtmlcode);
				
			} catch (ComparisonFailure e) {
				log.error(e.getMessage());
			}
			String pathCode = sheet1.getRow(i).getCell(6).getStringCellValue();
			CellType celltype = sheet1.getRow(i).getCell(7).getCellType();
			String pathcodevalue =null;
			if (celltype.equals(CellType.STRING)) {
				 pathcodevalue = sheet1.getRow(i).getCell(7).getStringCellValue();
			}else if (celltype.equals(CellType.NUMERIC)) {
				long l = (long)sheet1.getRow(i).getCell(7).getNumericCellValue();
				pathcodevalue = String.valueOf(l);
			}
			
			
			String responsemessgae = sheet1.getRow(i).getCell(8).getStringCellValue();
			String responsemessgaeValue = sheet1.getRow(i).getCell(9).getStringCellValue();

			
			
			log.debug("Expected path code "+pathCode);
			log.debug("Expected path value "+ pathcodevalue);

			log.debug("Expected Response message "+responsemessgae);
			log.debug("Expected Response message value "+responsemessgaeValue);
			
			

			JsonPath jsonPath = get.getBody().jsonPath();
			Object actualpathValue = jsonPath.get(pathCode);
			String actualresponseValue = jsonPath.get(responsemessgae);
			try {
				org.junit.Assert.assertEquals(pathcodevalue, String.valueOf(actualpathValue));
				
			} catch (ComparisonFailure e) {
				log.error(e.getMessage());
			}
			try {
				org.junit.Assert.assertEquals(responsemessgaeValue, actualresponseValue);
				
			} catch (Throwable e) {
				log.error(e.getMessage());
			}

			
			log.debug("Actual path value "+actualpathValue);
			log.debug("Actual response value "+actualresponseValue);

			
			if(!responsemessgaeValue.equals(actualresponseValue)) {
				//com.TEAF.stepDefinition.Hooks.scenario.write("\tExpected: "+pathcodevalue+" but found: "+pathValue);
				//com.TEAF.stepDefinition.Hooks.scenario.write("\tExpected: "+pathcodevalue+" but found: "+responseValue);
				isfailure="true";
			}
			resultlist.add(isfailure);
			
		}
		assertFalse(resultlist.contains("true"));
	}

}
