package com.TEAF.stepDefinition;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;
import java.util.Map.Entry;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.opencsv.CSVReader;

public class CsvToExcel {

	public static final char FILE_DELIMITER = ',';
	public static final String FILE_EXTN = ".xlsx";
	public static final String FILE_NAME = "MattressFirm";

	private static Logger logger = Logger.getLogger(CsvToExcel.class);

	public static String convertCsvToXls(String xlsFileLocation, String csvFilePath) {
		SXSSFSheet sheet = null;
		CSVReader reader = null;
		Workbook workBook = null;
		String generatedXlsFilePath = "";
		FileOutputStream fileOutputStream = null;

		try {

			/**** Get the CSVReader Instance & Specify The Delimiter To Be Used ****/
			String[] nextLine;
			reader = new CSVReader(new FileReader(csvFilePath), FILE_DELIMITER);

			workBook = new SXSSFWorkbook();
			sheet = (SXSSFSheet) workBook.createSheet("Sheet");

			int rowNum = 0;
			logger.info("Creating New .Xls File From The Already Generated .Csv File");
			while((nextLine = reader.readNext()) != null) {
				Row currentRow = sheet.createRow(rowNum++);
				for(int i=0; i < nextLine.length; i++) {
					if(NumberUtils.isDigits(nextLine[i])) {
						currentRow.createCell(i).setCellValue(Integer.parseInt(nextLine[i]));
					} else if (NumberUtils.isNumber(nextLine[i])) {
						currentRow.createCell(i).setCellValue(Double.parseDouble(nextLine[i]));
					} else {
						currentRow.createCell(i).setCellValue(nextLine[i]);
					}
				}
			}

			generatedXlsFilePath = xlsFileLocation + FILE_NAME + FILE_EXTN;
			logger.info("The File Is Generated At The Following Location?= " + generatedXlsFilePath);

			fileOutputStream = new FileOutputStream(generatedXlsFilePath.trim());
			workBook.write(fileOutputStream);
		} catch(Exception exObj) {
			logger.error("Exception In convertCsvToXls() Method?=  " + exObj);
		} finally {			
			try {

				/**** Closing The Excel Workbook Object ****/
				workBook.close();

				/**** Closing The File-Writer Object ****/
				fileOutputStream.close();

				/**** Closing The CSV File-ReaderObject ****/
				reader.close();
			} catch (IOException ioExObj) {
				logger.error("Exception While Closing I/O Objects In convertCsvToXls() Method?=  " + ioExObj);			
			}
		}

		return generatedXlsFilePath;
	}	
	
	
	
	public static void updateCustomizedExcel() throws Throwable {

		File f = new File("D:\\Child TEAF\\childteaf\\childteaf\\src\\test\\java\\com\\TEAF\\json\\MattressFirm.xlsx");
		FileInputStream fin = new FileInputStream(f);
		Workbook wb = new XSSFWorkbook(fin);

		Sheet sheetAt = wb.getSheetAt(0);
		HashMap<String, String> hm = new HashMap<String, String>();
		System.out.println(sheetAt.getPhysicalNumberOfRows());
		for (int i = 1; i < sheetAt.getPhysicalNumberOfRows(); i++) {

			String cid = sheetAt.getRow(i).getCell(0).getStringCellValue();
			String aid = sheetAt.getRow(i).getCell(1).getStringCellValue();

			String ln = sheetAt.getRow(i).getCell(2).getStringCellValue();
			String fn = sheetAt.getRow(i).getCell(3).getStringCellValue();

			String mcid = sheetAt.getRow(i).getCell(sheetAt.getRow(i).getPhysicalNumberOfCells() - 1)
					.getStringCellValue();
			if (mcid.length() < 1) {
				mcid = "null";
			}
			hm.put(ln + fn, cid + "=" + aid + "=" + mcid);

		}
		System.out.println(hm.size());

		File of = new File(
				"D:\\Child TEAF\\childteaf\\childteaf\\src\\test\\java\\com\\TEAF\\json\\MattressFirm_DT.xlsx");
		FileInputStream ofin = new FileInputStream(of);
		Workbook owb = new XSSFWorkbook(ofin);
		Sheet sheetAt2 = owb.getSheetAt(0);
		Set<Entry<String, String>> entrySet = hm.entrySet();
		int rownum = 1;
		for (Entry<String, String> x : entrySet) {
			String value = x.getValue();
			System.out.println(value);
			String[] split = value.split("=");
			Row row = sheetAt2.getRow(rownum);
			if (row == null) {
				row = sheetAt2.createRow(rownum);
			}
			Cell mid = row.getCell(3);
			if (mid == null) {
				mid = row.createCell(3);
			}

			Cell cdi = row.getCell(5);
			if (cdi == null) {
				cdi = row.createCell(5);
			}

			Cell mcid = row.getCell(7);
			if (mcid == null) {
				mcid = row.createCell(7);
			}

			mid.setCellValue(split[1]);
			cdi.setCellValue(split[0]);

			mcid.setCellValue(split[2]);

			rownum++;

		}
		FileOutputStream fout = new FileOutputStream(of);
		owb.write(fout);
		owb.close();

	}
}