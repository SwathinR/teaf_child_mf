package com.TEAF.TestFiles.Runner;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.TEAF.Hooks.Hooks;
import com.TEAF.framework.GenerateCustomReport;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.TestConfig;
import com.TEAF.framework.Utilities;
import com.TEAF.stepDefinitions.GalenStep;
import com.galenframework.config.GalenConfig;
import com.galenframework.config.GalenProperty;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@CucumberOptions(dryRun = false, plugin = { "com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:",
		"pretty",
		// "html:CukeNativeHTMLReport/",
		// "junit:src/test/java/com/TestResults/cucumber-report/cucumber_1.xml",
		"json:src/test/java/com/TestResults/cucumber-report/cucumber_1.json" }, strict = true, junit = "--step-notifications", features = {
				"src/test/java/com/TEAF/TestFiles/Features" },
		glue = { "com.TEAF.Hooks","com.TEAF.stepDefinition" }, tags = { "@PostMerge", "not @Ignore" }, monochrome = true)

@RunWith(Cucumber.class)
public class TestRunner_1 {

	static String Platform = "desktop";
//	static String Browser = "browserstack-desktop-browser-local";
	static String Browser = "REST Service";
	public static String TestName = "RC TEAF";

	static String reportPath = null;
	static String appType = "webapp";
	static String appLocation;

	@BeforeClass
	public static void setUp() {
		try {
			// LoadConfigurations
			TestConfig.LoadAllConfig();
			// Platform = System.getProperty("PlatformName");
			Browser = System.getProperty("BrowserName", Browser);
			appType = System.getProperty("AppType", appType);
			appLocation = System.getProperty("AppLocation", "D:/Omega Syteline/SyteLine.application");

			// System.setProperty("test.platformName", Platform);
			System.setProperty("test.browserName", Browser);
			System.setProperty("test.appType", appType);
			System.setProperty("test.winDesktopApp", appLocation);
			String URL = System.getProperty("URL", "https://obuat.net.au/shop/en/albany/");
			System.setProperty("test.appUrl", URL);

			System.setProperty("test.TestName", TestName);

			// Report configurations
			DateFormat dateFormat = new SimpleDateFormat("yy_MM_dd__HH_mm_ss");
			Date date = new Date();
			String timeStamp = dateFormat.format(date);
			reportPath = GenerateCustomReport.reportPath;

			if (System.getProperty("test.disableExtentReport").equalsIgnoreCase("false")) {
				System.setProperty("screenshot.dir", reportPath + "/Screenshots/");
				System.setProperty("extent.reporter.html.start", "true");
				System.setProperty("extent.reporter.spark.start", "true");
				System.setProperty("extent.reporter.html.out", reportPath + "/ExtentTestReport.html");
				System.setProperty("extent.reporter.spark.out", reportPath + "/");

				// System.setProperty("extent.reporter.logger.start", "true");
				// System.setProperty("extent.reporter.html.config","src/test/java/com/Resources/extent-config.xml");
				// System.setProperty("extent.reporter.logger.out", "output/LoggerOutput/");
				// System.setProperty("extent.reporter.klov.start", "true");
				// System.setProperty("extent.reporter.klov.config", "klov.properties");
			}

			// Galen Property
			GalenConfig.getConfig().setProperty(GalenProperty.SCREENSHOT_FULLPAGE, "true");

			// ScreenVideoCapture
			if (System.getProperty("test.disableScreenVideoCapture").equalsIgnoreCase("false")) {
				Utilities.startVideoRecorder();
			}
			// SessionSetup
			System.out.println("Platform: " + Platform);
			System.out.println("Browser : " + Browser);
			if (Platform.equals("desktop") && !Browser.equals("REST Service")) {
				//StepBase.setUp(Platform, Browser);
			} else if (Platform.equals("mobile") && !Browser.equals("REST Service")) {
				// StepBase.appiumStart(); //Appium server Should be installed via node js for
				// this to work
				//StepBase.setUp(Platform, Browser);
			} else {
				// System.out.println("Enter valid platform choice: desktop / android / ios");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AfterClass
	public static void tearDown() throws Throwable {
		try {
			// WrapperFunctions.sendMail("C:\\Users\\user\\workspace\\CucumberFramework\\output\\Run_1475150446542\\report.html",
			// "swathin@royalcyber.com");
			if (System.getProperty("test.disableGalenReport").equalsIgnoreCase("false")) {
				GalenStep.generateUIReport();

			}
			StepBase.tearDown();

			// Generation of Default Cucumber Reports
			if (!System.getProperty("test.disableCucumberReport").equalsIgnoreCase("true")) {
				// Cucumber Report Generation
				GenerateCustomReport.generateCustomeReport(Browser, Platform, "cucumber_1.json");
				HashMapContainer.ClearHM();
			}

			// uncomment before pushing

//			File src = new File(System.getProperty("user.dir")+"\\src\\test\\java\\com\\Resources\\OmegaTestDatas.xlsx");
//			String pathname = System.getProperty("ExcelPath", "c:\"");
//			File des = new File(pathname+"\\OmegaTestDatas.xlsx");
//			FileUtils.copyFile(src, des);
//			if(des.isFile()) {
//				System.out.println("File Copied");
//			}
//			

			Collection<String> values = Hooks.scenarioStatus.values();
			String toMailId = null;
			String ccMailId = null;
			boolean flag = false;
			System.out.println(values);
			if ((values.contains("FAILED") || values.contains("SKIPPED"))
					&& !(Hooks.code == 520 || Hooks.code == 521)) {
				flag = true;
			}

			System.out.println("Failure Present: " + flag);
			if (flag == true) {
				// Mailing only internal team to review the failures

				toMailId = "natasha@royalcyber.com,zainab.f@royalcyber.com,logeshkumar.r@royalcyber.com,chandan.navara@royalcyber.com,swathin@royalcyber.com";
				ccMailId = "premkumar.g@royalcyber.com";
			} else {
				// Mailing everyone as results look good
				toMailId = "premkumar.g@royalcyber.com";
				ccMailId = "premkumar.g@royalcyber.com";
				}

			// Prepare reports for Email
			if (System.getProperty("test.generateEmail").equalsIgnoreCase("true")) {
				String toMail = System.getProperty("ToMailID", toMailId);
				String ccMail = System.getProperty("CCMailID", ccMailId);
				Utilities.reportstoZipFile(GenerateCustomReport.reportPath, "TestExecution_ExtentReports");
				Utilities.reportstoZipFile("UITestReports", "TestExecution_UIReports");
//				System.out.println(toMailId);
//				System.out.println(ccMailId);
				Utilities.auto_generation_Email(toMailId, ccMailId);
			//	 Utility.auto_generateEmail(toMail, ccMail);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
