@MF_Sanity
Feature: Sanity Test - Mulesoft PreMerge & PostMerge End Points

Background: Placemaker Application URL
Given URL 'https://proc-cloudingo-sf-merge.us-w2.cloudhub.io'

@PreMerge 
Scenario Outline: PreMerge Get Request to SalesForce from Mulesoft  (All Combination of Query Parameter values - Positive & Negative)
	Given EndPoint '/api/sfcloumerge'
	And Set Header key 'client_id' & 'client_secret' values
	And Query params with Key 'Action' and value '<Param_Action>'
	And Query params with Key 'mid' and value '<Param_mid>'
	And Query params with Key 'cids' and value '<Param_cids>'
	And Query params for random tid
	And Query params with Key 'mcustid' and value '<Param_mcustid>'
	And Query params with Key 'reason' and value '<Param_reason>'
	And Method 'Get'
	And Statuscode '<httpcode>'
	And Match JSONPath '<PathStatusCode>' contains '<StatusCodeValue>'
	And Match JSONPath '<ResponseBodypath>' contains '<ResponseBodymessage>'
	And Print response in report
	
Examples: 
|Param_Action |Param_mid		 |Param_cids		|Param_mcustid|Param_reason							   |httpcode|PathStatusCode|StatusCodeValue|ResponseBodypath|ResponseBodymessage							 |
|PreMerge	  |0012300000QGTFdAAP|0032300000NVtbjAAD|ST-4304209	  |Verify 200 Valid request success message|200		|statusCode    |SFMERGE-200	   |message		 	|Request received. Processing your request.		 |
|PreMerge	  |Not Specified	 |0032300000NVtbjAAD|ST-4304209	  |Verify 200 Valid request success message|450 	|errorCode     |450			   |errorMessage	|"Required query parameter mid not specified"	 |
|Not Specified|0012300000QGTFdAAP|0032300000NVtbjAAD|ST-4304209	  |Verify 200 Valid request success message|450		|errorCode     |450			   |errorMessage	|"Required query parameter Action not specified."|

@PostMerge
Scenario Outline: PreMerge Get Request to SalesForce from Mulesoft  (All Combination of Query Parameter values - Positive & Negative)
	Given EndPoint '/api/sfcloumerge'
	And Set Header key 'client_id' & 'client_secret' values
	And Query params with Key 'Action' and value '<Param_Action>'
	And Query params with Key 'mid' and value '<Param_mid>'
	And Query params with Key 'cids' and value '<Param_cids>'
	And Query params with Key 'tid' and value '<Param_tid>'
	And Query params with Key 'mcustid' and value '<Param_mcustid>'
	And Query params with Key 'reason' and value '<Param_reason>'
	And Method 'Get'
	And Statuscode '<httpcode>'
	And Match JSONPath '<PathStatusCode>' contains '<StatusCodeValue>'
	And Match JSONPath '<ResponseBodypath>' contains '<ResponseBodymessage>'
	And Print response in report
	
Examples:	
|Param_Action  |Param_mid		  |Param_cids							|Param_tid|Param_mcustid|Param_reason		  |httpcode |PathStatusCode|StatusCodeValue|ResponseBodypath|ResponseBodymessage|
|MergeSucceeded|0032300000NVtbqAAD|0032300000Lq6IOAAZ,0032300000NVtRMAA1|9e353b78-80f0-4afe-add1-2a603bb3d9de|ST-4998207   |Use+for+Mulesoft+Test|200		|statusCode    |SFMERGE-200	   |message		 	|Merge Succeeded	|
